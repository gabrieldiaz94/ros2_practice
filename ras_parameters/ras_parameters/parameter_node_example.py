import rclpy
from rcl_interfaces.msg import ParameterDescriptor

global g_node

def timer_callback():
    global g_node
    # get parameter
    my_parameter = g_node.get_parameter('my_param').get_parameter_value().integer_value
    g_node.get_logger().info(' Hola el valor del parametro es %d' % my_parameter)
    # set parameter
    my_new_parameter = rclpy.Parameter('my_param', rclpy.Parameter.Type.INTEGER, 150)
    all_parameters = [my_new_parameter]
    g_node.set_parameters(all_parameters)


def main(args=None):
    # Init rclpy
    rclpy.init(args=args)
    # Create a node
    global g_node
    g_node = rclpy.create_node('node_param_example')
    parameter_descriptor = ParameterDescriptor(description = 'This is an integer value')
    parameter = g_node.declare_parameter('my_param',10, parameter_descriptor)
    timer = g_node.create_timer(1, timer_callback)

    # Spin
    rclpy.spin(g_node)
    # Close node
    rclpy.shutdown()

if __name__ == '__main__':
    main()
