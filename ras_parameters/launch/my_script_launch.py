import launch
import launch_ros.actions

def generate_launch_description():
    return launch.LaunchDescription([
        # launch_ros.actions.Node(
        #     package='ras_parameters',
        #     executable='ras_param',
        #     name='param_example',
        #     parameters= [
        #         {'my_param' : 200},
        #     ]
        # ),
        # launch_ros.actions.Node(
        #     package='turtlesim',
        #     executable='turtlesim_node',
        #     name='turtlesim_node',
        #     parameters=[
        #         { 'background_b' : 100},
        #         { 'background_r' : 10},
        #         { 'background_g' : 50},]
        # ),
        launch_ros.actions.Node(
            package='ras2_topics',
            executable='publicar',
            name='publisher',
            output='screen',
            emulate_tty = True
        ),
        launch_ros.actions.Node(
            package='ras2_topics',
            executable='subscriptor',
            name='subscriber',
            output='screen',
            emulate_tty = True
        ),
    ])