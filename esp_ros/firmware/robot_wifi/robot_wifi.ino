#include <ESP8266WiFi.h>
#include <Servo.h>

int port = 8144;
WiFiServer server(port);
Servo motor;

const char *ssid = "rasJaveriana";
const char *passwd = "rasjaveriana2023";

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  //Servo
  motor.attach(0);
  motor.write(90);
  
  // mode 
  WiFi.mode(WIFI_STA);
  // begin
  WiFi.begin(ssid,passwd);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  server.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  WiFiClient client = server.available();
  String read_ros = "";
  if(client) {
    if(client.connected()) {
      Serial.println("Client Connected");
    }
    
    while(client.connected()){
      //Read
      while(client.available() > 0) {
        char data = client.read();
        read_ros.concat(data);
      }

      if(read_ros.compareTo("") != 0) {//SI llego un mensaje
        Serial.println(read_ros);
        int x = read_ros.toInt();
    
        //Servo
        motor.write(x);    
        //Led
        /*if( x != 0 ) { // Enciendo el led
          digitalWrite(LED_BUILTIN, LOW);
        } else {
          digitalWrite(LED_BUILTIN, HIGH);
        }*/
        read_ros = "";
      }

      //Write
      while(Serial.available()>0){
        char c = Serial.read();
        client.print(c);
      }
    }
    client.stop();
    Serial.println("Client disconnected");
  }
}
