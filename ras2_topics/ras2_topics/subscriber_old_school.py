# Copyright 2016 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rclpy

from std_msgs.msg import String
from turtlesim.msg import Pose
from ras_interface.msg import Num


def callback_msg(msg):
    print(msg.num)

def callback_turtlesim(msg):
    print(str(msg.x) + ' '+ str(msg.y) + ' ' + str(msg.theta))

def main(args=None):
    rclpy.init(args=args)

    g_node = rclpy.create_node('minimal_subscriber')

    subscription = g_node.create_subscription(Num, '/publicador', callback_msg, 10)
    turtlesim_subscriber = g_node.create_subscription(Pose,'/turtle1/pose', callback_turtlesim, 10)
    subscription  # prevent unused variable warning
    turtlesim_subscriber # prevent

    rclpy.spin(g_node)
    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    g_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
