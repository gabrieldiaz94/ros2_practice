import rclpy
from std_msgs.msg import String
import serial

global g_node,arduino_
def msg_callback(msg):
    global g_node,arduino_
    
    g_node.get_logger().info("New Message received, publishing on serial: %s" % arduino_.name)
    arduino_.write(msg.data.encode("utf-8"))

def main():
    rclpy.init()
    global g_node, arduino_
    g_node = rclpy.create_node('simple_serial_transmitter')
    g_node.declare_parameter("port", "/dev/ttyACM0")
    g_node.declare_parameter("baud_rate", 115200)
    port = g_node.get_parameter("port").value
    baud_rate = g_node.get_parameter("baud_rate").value
    arduino_ = serial.Serial(port=port, baudrate=baud_rate, timeout=0.1)
    subscription = g_node.create_subscription(String, "serial_transmitter", msg_callback, qos_profile=10)
    rclpy.spin(g_node)
    rclpy.shutdown()

if __name__ == '__main__':
    main()
