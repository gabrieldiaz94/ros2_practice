import rclpy

from std_msgs.msg import String
import serial

global g_node, simple_serial_receiver, arduino_

def timerCallback():
    global g_node, simple_serial_receiver, arduino_
    if rclpy.ok() and arduino_.is_open:
        data = arduino_.readline()
        try:
            data.decode("utf-8")
        except:
            return
        msg = String()
        msg.data = str(data)
        simple_serial_receiver.publish(msg)


def main():
    rclpy.init()
    global g_node, simple_serial_receiver, arduino_
    g_node = rclpy.create_node('simple_publisher')
    g_node.declare_parameter("port", "/dev/ttyACM0")
    g_node.declare_parameter("baud_rate", 115200)
    port = g_node.get_parameter("port").value
    baud_rate = g_node.get_parameter("baud_rate").value
    arduino_ = serial.Serial(port=port, baudrate=baud_rate, timeout=0.1)
    simple_serial_receiver = g_node.create_publisher(String, '/serial_receiver', 10)
    timer = g_node.create_timer(0.01, timerCallback)
    rclpy.spin(g_node)
    rclpy.shutdown()


if __name__ == '__main__':
    main()
