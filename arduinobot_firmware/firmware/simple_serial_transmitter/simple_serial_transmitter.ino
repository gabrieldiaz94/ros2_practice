int x = 0; // Value to publish
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.setTimeout(1);
}

void loop() {
  // put your main code here, to run repeatedly:
  //Message to publish
  Serial.println(x);
  //Increase counter
  x++;

  delay(0.1);
}
