#include <Servo.h>

#define servo_pin 8
Servo motor;


void setup() {
  // put your setup code here, to run once:
  motor.attach(servo_pin); // Pass the number of
                           // the pin for the servomotor
  motor.write(90); // Pass the angle in degrees

  Serial.begin(115200); // Set Baut rate
  Serial.setTimeout(1); // Set timeout
}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available()) {
    int angle = Serial.readString().toInt();
    motor.write(angle);
  }

  delay(0.1);
}
