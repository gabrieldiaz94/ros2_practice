#define LED_PIN 13 // Integrated Led

void setup() {
  // put your setup code here, to run once:
  pinMode(LED_PIN, OUTPUT);
  //OUTPUT -> Something that you control
  //INPUT -> Something that you need to adquire
  digitalWrite(LED_PIN, LOW); // Turn off the led

  Serial.begin(115200); // BaudRate
  Serial.setTimeout(1); // For the serial Communication
}


void loop() {
  // put your main code here, to run repeatedly:
  //After Setup
  if(Serial.available()) { // ROS SEND a message trought arduino
     int x = Serial.readString().toInt(); // Asuming that we receive an Integer
     if(x==0){
       digitalWrite(LED_PIN, LOW); // Turn off the led    
     } else {
      digitalWrite(LED_PIN, HIGH); // Turn on the led
     }
  }

  delay(0.1); // To decrease the speed of the loop
}
